import os
import cv2
from PIL import Image
import numpy as np
import pickle

face_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt2.xml')
cat_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalcatface.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

image_dir = os.path.join(BASE_DIR, "images")

curr_id = 0
label_ids ={}

y_labels = []
x_train = []

for roots, dirs, files in os.walk(image_dir):
    for file in files:
        if file.endswith("png") or file.endswith("jpg"):
            path = os.path.join(roots, file)
            label = os.path.basename(os.path.dirname(path)).replace(" ", "-").lower()
            if not label in label_ids:
                label_ids[label] = curr_id
                curr_id += 1
            id_ = label_ids[label]
            

            pil_image = Image.open(path).convert("L") # convert into grayscale
            # pil_image = Image.open(path)

            size = (550, 550)
            final_image = pil_image.resize(size, Image.ANTIALIAS)
            image_array = np.array(np.asarray(final_image), dtype="uint8")
            faces = face_cascade.detectMultiScale(image_array, scaleFactor=1.5, minNeighbors=5)
            for (x, y, w, h) in faces:
                # region of interest
                roi =  image_array[y:y+h, x:x+w]
                x_train.append(roi)
                y_labels.append(id_)

            cats = cat_cascade.detectMultiScale(image_array, scaleFactor=1.5, minNeighbors=5)
            for(xc, yc, wc, hc) in cats:
                # region of interest
                roi =  image_array[y:y+h, x:x+w]
                x_train.append(roi)
                y_labels.append(id_)

with open("labels.pickle", "wb") as f:
    print(label_ids)
    pickle.dump(label_ids, f)

print(x_train, y_labels)
recognizer.train(x_train, np.array(y_labels))
recognizer.save("trainer.yml")