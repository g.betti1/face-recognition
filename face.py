import numpy as nb
import cv2
import pickle

face_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt2.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")
cat_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalcatface.xml')
cat_cascade2 = cv2.CascadeClassifier('cascades/haarcascade_frontalcatface_extended.xml')

print(face_cascade)

cap = cv2.VideoCapture(0)

labels = {"person_name":1}
with open("labels.pickle", "rb") as f:
    og_labels = pickle.load(f)
    labels = {v:k for k,v in og_labels.items()}

while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5)

    for (x, y, w, h) in faces:
        # region of interest
        roi =  gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]

        id_, conf = recognizer.predict(roi)

        b_box = "b_box.png"
        full_image = "full_image.png"
        cv2.imwrite(b_box, roi)
        cv2.imwrite(full_image, gray)

        font = cv2.FONT_HERSHEY_SIMPLEX
        name = labels[id_]
        color = (255,255,255)
        stroke = 2
        cv2.putText(frame, name, (x,y - 15), font, 1, color, stroke, cv2.LINE_AA)

        color = (255,0,0) # BGR color
        stroke = 2
        end_cord_x = x + w
        end_cord_y = y + h
                                    # width        height
        cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
    
    cats = cat_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5)
    for(xc, yc, wc, hc) in cats:
        cv2.rectangle(gray, (x, y), (end_cord_x, end_cord_y), (0,255,0), stroke)

    cats = cat_cascade2.detectMultiScale(frame, scaleFactor=1.5, minNeighbors=5)
    for(xc, yc, wc, hc) in cats:
        cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), (0,255,0), stroke)
        
    # Display the resulting frame
    cv2.imshow('frame', frame)
    if cv2.waitKey(20) & 0xFF == ord('q'):
        break

# release the capture
cap.release()
cv2.destroyAllWindows()